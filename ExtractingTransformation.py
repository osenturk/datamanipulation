# Import pandas
import pandas as pd

# Read in filename and set the index: election
election = pd.read_csv("data/pennsylvania2012_turnout.csv", index_col='county')

# Create a separate dataframe with the columns ['winner', 'total', 'voters']: results
results = election.loc[:,['winner', 'total', 'voters']]

# Print the output of results.head()
print(results.head())

p_counties=election.loc['Perry':'Potter']

print(p_counties)

## SLICING ROWS REVERSE ORDER
p_counties_rev=election.loc['Potter':'Perry':-1]

print(p_counties_rev)

## SLICING COLUMNS
# Slice the columns from the starting column to 'Obama': left_columns
left_columns = election.loc[:,:'Obama']

# Print the output of left_columns.head()
print(left_columns.head())

# Slice the columns from 'Obama' to 'winner': middle_columns
middle_columns = election.loc[:,'Obama':'winner']

# Print the output of middle_columns.head()
print(middle_columns.head())

# Slice the columns from 'Romney' to the end: 'right_columns'
right_columns = election.loc[:,'Romney':]

# Print the output of right_columns.head()
print(right_columns.head())

# Create the list of row labels: rows
rows = ['Philadelphia', 'Centre', 'Fulton']

# Create the list of column labels: cols
cols = ['winner', 'Obama', 'Romney']

# Create the new DataFrame: three_counties
three_counties = election.loc[rows,cols]

# Print the three_counties DataFrame
print(three_counties)

## SLICING WITH BOOLEAN FILTER
# Create the boolean array: high_turnout
high_turnout = election.loc[:,'turnout'] > 70

# Filter the election DataFrame with the high_turnout array: high_turnout_df
high_turnout_df = election[high_turnout]

# Print the high_turnout_results DataFrame
print(high_turnout_df)

# Import numpy
import numpy as np

# Create the boolean array: too_close
too_close = election.loc[:,'margin'] < 1

# Assign np.nan to the 'winner' column where the results were too close to call
election[too_close] = np.nan

# Print the output of election.info()
print(election.info())


# Read in filename: titanic
titanic = pd.read_csv("data/titanic.csv")

print(titanic.info())
print(titanic.head())

## Filtering using NaNs, dropna
# ‘any’ : If any NA values are present, drop that row or column.
# ‘all’ : If all values are NA, drop that row or column.

# Select the 'age' and 'cabin' columns: df
df = titanic.loc[:,['age','cabin']]

# Print the shape of df
print(df.shape)

# Drop rows in df with how='any' and print the shape
print(df.dropna(how='any').shape)

# Drop rows in df with how='all' and print the shape
print(df.dropna(how='all').shape)

# Drop columns in titanic with less than 1000 non-missing values
print(titanic.dropna(thresh=1000, axis='columns').info())

## applying function
# Read in filename: weather
weather = pd.read_csv("data/pittsburgh2013.csv")

# Write a function to convert degrees Fahrenheit to degrees Celsius: to_celsius
def to_celsius(F):
    return 5/9*(F - 32)

# Apply the function over 'Mean TemperatureF' and 'Mean Dew PointF': df_celsius
df_celsius = weather.loc[:,['Mean TemperatureF','Mean Dew PointF']].apply(to_celsius)
print(df_celsius.head())
# Reassign the columns df_celsius
df_celsius.columns = ['Mean TemperatureC', 'Mean Dew PointC']

# Print the output of df_celsius.head()
print(df_celsius.head())

## Using .map() with dictionary for column value conversions
# Create the dictionary: red_vs_blue
red_vs_blue = {'Obama':'blue','Romney':'red'}

# Use the dictionary to map the 'winner' column to the new column: election['color']
election['color'] = election.loc[:,'winner'].map(red_vs_blue)

# Print the output of election.head()
print(election.head())

## Using vectorized functions (called Universal Functions or UFuncs in NumPy).
# Import zscore from scipy.stats
from scipy.stats import zscore

# Call zscore with election['turnout'] as input: turnout_zscore
turnout_zscore = zscore(election['turnout'])

# Print the type of turnout_zscore
print(type(turnout_zscore))

# Assign turnout_zscore to a new column: election['turnout_zscore']
election['turnout_zscore'] = turnout_zscore

# Print the output of election.head()
print(election.head())