import pandas as pd
import matplotlib.pyplot as plt

## Group by: Grouping by multiple columns

medals = pd.read_csv('data/all_medalists.csv')
print(medals)
print(medals.info())


## Using .value_counts() for ranking. Also same result with group by

# Notice that .value_counts() sorts by values by default.
# The result is returned as a Series of counts indexed by unique entries
# from the original Series with values (counts) ranked in descending order

# Select the 'NOC' column of medals: country_names
country_names = medals.loc[:,'NOC']

# Count the number of medals won by each country: medal_counts
medal_counts = country_names.value_counts()

# Print top 15 countries ranked by medals
print(medal_counts.head(15))

by_me = medals.groupby('NOC')['Medal'].count().sort_values(ascending=False)
print(by_me.head(15))

## Using .pivot_table() to count medals by type

counted = medals.pivot_table(values='Athlete',index='NOC',columns='Medal',aggfunc='count')

counted['totals'] = counted.sum(axis='columns')

counted = counted.sort_values(by='totals',ascending=False)

print(counted.head(15))

## Applying .drop_duplicates, unique elements in a data frame

ev_gen = medals.loc[:,['Event_gender', 'Gender']]

ev_gen_uniques = ev_gen.drop_duplicates()

print(ev_gen_uniques)

## Finding possible errors with .groupby()

medals_by_gender = medals.groupby(['Event_gender','Gender'])

medal_count_by_gender = medals_by_gender.count()

print(medal_count_by_gender)

## boolean filtering
# Create the Boolean Series: sus
# Create the Boolean Series: sus
sus =(medals.Event_gender == 'W') & (medals.Gender == 'Men')

# Create a DataFrame with the suspicious row: suspect
suspect = medals[sus]

# Print suspect
print(suspect)

## Using .nunique() to rank by distinct sports : similar to Group by

country_grouped = medals.groupby(['NOC'])

Nsports = country_grouped['Medal'].nunique()

Nsports = Nsports.sort_values(ascending=False)

print(Nsports.head(15))

## Aggregating, Group by, Boolean filtering, between and 'isin' look like nice commands

#during_cold_war = medals.loc[:,'Edition'].between(1952,1988,inclusive=True)

# Extract all rows for which the 'Edition' is between 1952 & 1988: during_cold_war
during_cold_war = (medals.loc[:,'Edition']>=1952) &  (medals.loc[:,'Edition']<=1988)

# Extract rows for which 'NOC' is either 'USA' or 'URS': is_usa_urs
is_usa_urs = medals.NOC.isin(['USA','URS'])

# Use during_cold_war and is_usa_urs to create the DataFrame: cold_war_medals
cold_war_medals = medals.loc[during_cold_war & is_usa_urs]

# Group cold_war_medals by 'NOC'
country_grouped = cold_war_medals.groupby('NOC')

# Create Nsports
Nsports = country_grouped['Sport'].nunique().sort_values(ascending=False)

# Print Nsports
print(Nsports)

## Groupby, Filtering, slicing, idxmax command

# Create the pivot table: medals_won_by_country
medals_won_by_country = medals.pivot_table(values='Medal',index='Edition', columns='NOC',fill_value=None,aggfunc='count')

# Slice medals_won_by_country: cold_war_usa_urs_medals
cold_war_usa_urs_medals = medals_won_by_country.loc[1952:1988, ['USA','URS']]

# Create most_medals
most_medals = cold_war_usa_urs_medals.idxmax(axis='columns')

# Print most_medals.value_counts()
print(most_medals.value_counts())

## Visualization: unstack indexes come from pivot_table

# usa = medals[medals.loc[:,'NOC']=='USA']
usa = medals[medals['NOC']=='USA']

print(usa)

usa_medals_by_year = usa.groupby(['Edition','Medal'])['Athlete'].count()

print(usa_medals_by_year)

usa_medals_by_year = usa_medals_by_year.unstack(level='Medal')

print(usa_medals_by_year)

usa_medals_by_year.plot()
plt.show()

## Visualization: USA Medal Counts by Edition: Area Plot

# Create the DataFrame: usa
usa = medals[medals.NOC == 'USA']

# Group usa by 'Edition', 'Medal', and 'Athlete'
usa_medals_by_year = usa.groupby(['Edition', 'Medal'])['Athlete'].count()

# Reshape usa_medals_by_year by unstacking
usa_medals_by_year = usa_medals_by_year.unstack(level='Medal')

# Create an area plot of usa_medals_by_year
usa_medals_by_year.plot.area()

plt.show()

## Visualization: Area Plot, Categorical, Category, Type Conversion

# Redefine 'Medal' as an ordered categorical
# medals.Medal = medals.Medal.astype('Medal')
medals.Medal = pd.Categorical(values=medals.Medal, categories=['Bronze', 'Silver', 'Gold'], ordered=True)

# Create the DataFrame: usa
usa = medals[medals.NOC == 'USA']

# Group usa by 'Edition', 'Medal', and 'Athlete'
usa_medals_by_year = usa.groupby(['Edition', 'Medal'])['Athlete'].count()

# Reshape usa_medals_by_year by unstacking
usa_medals_by_year = usa_medals_by_year.unstack(level='Medal')

# Create an area plot of usa_medals_by_year
usa_medals_by_year.plot.area()
plt.show()