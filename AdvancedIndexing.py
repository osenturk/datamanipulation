import pandas as pd

## changing index of a dataframe

sales = pd.read_csv('data/sales/sales.csv',index_col='month')

print(sales.head())

# Create the list of new indexes: new_idx
new_idx = [a.upper() for a in sales.index]

# Assign new_idx to sales.index
sales.index = new_idx

# Print the sales DataFrame
print(sales)

## Changing index name labels
# Assign the string 'MONTHS' to sales.index.name
sales.index.name = 'MONTHS'

# Print the sales DataFrame
print(sales)

# Assign the string 'PRODUCTS' to sales.columns.name
sales.columns.name='PRODUCTS'

# Print the sales dataframe again
print(sales)

sales = pd.read_csv('data/sales/sales.csv')
sales = sales.iloc[:,1:]

print(sales.head())

# Generate the list of months: months
months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun']

# Assign months to sales.index
sales.index = months

# Print the modified sales DataFrame
print(sales)

## Extracting data with a MultiIndex
sales = pd.read_csv('data/sales/sales_additional_index.csv',index_col=['state','month'])

print(sales.loc[['CA','TX']])
print(sales.loc['CA':'TX'])

sales = pd.read_csv('data/sales/sales_additional_index.csv')

# Set the index to be the columns ['state', 'month']: sales
sales = sales.set_index(['state','month'])

## Setting & sorting a MultiIndex
# Sort the MultiIndex: sales
sales = sales.sort_index()

# Print the sales DataFrame
print('before trial')
print(sales)

print('DF selection')
print(sales['eggs']['CA'])
print(sales.loc['CA','eggs'])

## stack trials
print(sales[1:0:-1])
print(sales[0:1])
print('stacked')
print(sales.stack())
print('unstacked')
print(sales.unstack())

print('unstacked state')
print(sales.unstack('state'))

print('unstacked month')
print(sales.unstack('month'))

## Using .loc[] with nonunique indexes
sales = pd.read_csv('data/sales/sales_additional_index.csv')
# Set the index to the column 'state': sales
sales = sales.set_index('state')

# Print the sales DataFrame
print(sales)

# Access the data from 'NY'
print(sales.loc['NY'])

## Indexing multiple levels of a MultiIndex, Multiple Index
sales = pd.read_csv('data/sales/sales_additional_index.csv',index_col=['state','month'])
print("Ozan")
print(sales)

# Look up data for NY in month 1: NY_month1
NY_month1 = sales.loc['NY',1]
print(NY_month1)

# Look up data for CA and TX in month 2: CA_TX_month2
CA_TX_month2 = sales.loc[(('CA','TX'),2),:]
print(CA_TX_month2)

# Look up data for all states in month 2: all_month2
print('slicing ..')
all_month2 = sales.loc[(slice(None),2),:]
print(all_month2)


print(sales["eggs"]["CA"])
print(sales.loc["CA","eggs"])