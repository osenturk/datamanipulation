import pandas as pd

## Pivoting a single variable

users = pd.read_csv('data/users.csv',index_col=0)

print(users)

visitors_pivot = users.pivot(index='weekday',columns='city',values='visitors')

print(visitors_pivot)

# Pivot users with signups indexed by weekday and city: signups_pivot
signups_pivot = users.pivot(index='weekday',columns='city',values='signups')

# Print signups_pivot
print(signups_pivot)

# Pivot users pivoted by both signups and visitors: pivot
pivot = users.pivot(index='weekday',columns='city')

## MultipleIndex MultipleLevel Print the pivoted DataFrame
print(pivot)
print('ozans')
extra=pivot.loc['Mon',['visitors']]

print(extra)
print(extra[:,'Dallas'])

## Stack Unstack I - MultiLevel indexes, Hierarchial
## drop columns without name
users = pd.read_csv('data/users.csv',index_col=['city','weekday'])
print(users)
# users = users.loc[('city','weekday'),:]
# users = users[('city','weekday'),:]
# users.drop(columns='a',inplace=True)
columns=[0]
users.drop(users.columns[columns],axis=1,inplace=True)

print(users)


# Unstack users by 'weekday': byweekday
print('unstacking...')
byweekday = users.unstack('weekday')

# Print the byweekday DataFrame
print(byweekday)

# Stack byweekday by 'weekday' and print it
print(byweekday.stack('weekday'))

## Stack Unstack II

# Unstack users by 'city': bycity
bycity = users.unstack('city')

# Print the bycity DataFrame
print('before stacking...')
print(bycity)

# Stack bycity by 'city' and print it
print('stacking...')
print(bycity.stack('city'))

## swap indexes and sort indexes

# Stack 'city' back into the index of bycity: newusers
newusers = bycity.stack('city')

# Swap the levels of the index of newusers: newusers
newusers = newusers.swaplevel(0,1)

# Print newusers and verify that the index is not sorted
print(newusers)

# Sort the index of newusers: newusers
newusers = newusers.sort_index()

# Print newusers and verify that the index is now sorted
print(newusers)

# Verify that the new DataFrame is equal to the original
print(newusers.equals(users))

## Melting
users = pd.read_csv('data/users.csv',index_col=['city','weekday'])

columns=[0]
users.drop(users.columns[columns],axis=1,inplace=True)
print(users)

visitors_by_city_weekday=users.unstack('city')
visitors_by_city_weekday = visitors_by_city_weekday.loc[:,'visitors']
print(visitors_by_city_weekday)

# Reset the index: visitors_by_city_weekday
visitors_by_city_weekday = visitors_by_city_weekday.reset_index()

# Print visitors_by_city_weekday
print(visitors_by_city_weekday)

# Melt visitors_by_city_weekday: visitors
visitors = pd.melt(visitors_by_city_weekday,id_vars=['weekday'],value_name='visitors')

# Print visitors
print(visitors)

## Melting: id_vars attribute keeps the value not to be melted
print("melting...")
users = pd.read_csv('data/users.csv')
columns=[0]
users.drop(users.columns[columns],axis=1,inplace=True)
print(users)

skinny = pd.melt(users,id_vars=['weekday','city'],value_name='value')
print(skinny)

users= users.set_index(['city', 'weekday'])
print(users)

## Melting multi - Obtaining key-value pairs with melt()
# If columns are a MultiIndex then use this level to melt.
print("melting to get key value pairs.......")
users = pd.read_csv('data/users.csv')
columns=[0]
users.drop(users.columns[columns],axis=1,inplace=True)
print(users)
# Set the new index: users_idx
users_idx = users.set_index(['city', 'weekday'])

# Print the users_idx DataFrame
print(users_idx)

# Obtain the key-value pairs: kv_pairs
print("just printing..")
kv_pairs = pd.melt(users_idx,col_level=0)

# Print the key-value pairs
print(kv_pairs)

## Pivot tables : When the index column combination is not unique
users = pd.read_csv('data/users.csv')
columns=[0]
users.drop(users.columns[columns],axis=1,inplace=True)
print(users)

# Create the DataFrame with the appropriate pivot table: by_city_day
by_city_day = users.pivot_table(index=['weekday'], columns='city')
# Print by_city_day
print(by_city_day)

# Use a pivot table to display the count of each column: count_by_weekday1
count_by_weekday1 = users.pivot_table(index='weekday',aggfunc='count')

print("pivoting table for weekday")
# Print count_by_weekday
print(count_by_weekday1)

# Replace 'aggfunc='count'' with 'aggfunc=len': count_by_weekday2
count_by_weekday2 = users.pivot_table(index='weekday',aggfunc=len)

# Verify that the same result is obtained
print('==========================================')
print(count_by_weekday1.equals(count_by_weekday2))

## Pivot tables: Using margins in pivot tables

# Create the DataFrame with the appropriate pivot table: signups_and_visitors
signups_and_visitors = users.pivot_table(index='weekday',aggfunc=sum)

# Print signups_and_visitors
print(signups_and_visitors)

# Add in the margins: signups_and_visitors_total
signups_and_visitors_total = users.pivot_table(index='weekday',aggfunc=sum, margins=True)

# Print signups_and_visitors_total
print(signups_and_visitors_total)

