import pandas as pd

df = pd.read_csv("data/dob_job_application_filings_subset.csv")

print(df.head())

print(df.tail())

print(df.shape)

print(df.columns)

print(df.describe())

### Frequency counts for categorical data

# Print the value counts for 'Borough'
print(df['Borough'].value_counts(dropna=False))

# Print the value_counts for 'State'
print(df["State"].value_counts(dropna=False))

# Print the value counts for 'Site Fill'
print(df["Site Fill"].value_counts(dropna=False))


### Visualization

import matplotlib.pyplot as plt

df["Existing Zoning Sqft"].describe()

df["Existing Zoning Sqft"].plot(kind='hist', rot=70, logx=True, logy=True)

plt.show()

print(df.head())
print(df.describe())


import seaborn as sns
titanic = pd.read_csv('data/titanic.csv',index_col=0)

by_sex = titanic.groupby('sex')['age'].count()
print(by_sex)
test = pd.DataFrame(by_sex)
print(test)
test.columns = ['gender']
print(test)
test.index= [0,1]
print(test)
test.plot(kind='bar')
plt.show()

# sns.countplot(x='sex',data=titanic)
#
# plt.show()

## build up data frames from the scratch
list_keys=['sex','age']
list_values=[('M','41'),('F','32')]
# Zip the 2 lists together into one list of (key,value) tuples: zipped
zipped = list(zip(list_keys,list_values))

# Inspect the list using print()
print(zipped)

# Build a dictionary with the zipped list: data
data = dict(zipped)

# Build and inspect a DataFrame from the dictionary: df
df = pd.DataFrame(data)
print(df)

# ## line charts
# # Create a list of y-axis column names: y_columns
# y_columns = ['AAPL','IBM']
#
# # Generate a line plot
# df.plot(x='Month', y=y_columns)
#
# # Add the title
# plt.title('Monthly stock prices')
#
# # Add the y-axis label
# plt.ylabel('Price ($US)')
#
# # Display the plot
# plt.show()

## box plots
cars = pd.read_csv('data/auto-mpg.csv')
print(cars.head())
# Make a list of the column names to be plotted: cols
cols = ['weight','mpg']

# Generate the box plots
cars[cols].plot(kind='box',subplots=True)

# Display the plot
plt.show()

## cumulative density functions

tips = pd.read_csv('data/tips.csv')
print(tips.head())

tips['fraction'] = tips.loc[:,'tip'] / tips.loc[:,'total_bill']



print(tips.head())

# This formats the plots such that they appear on separate rows
fig, axes = plt.subplots(nrows=2, ncols=1)

# Plot the PDF
tips.fraction.plot(ax=axes[0], kind='hist', x='pdf', normed=True,bins=30, range=(0,.3))


# Plot the CDF
tips.fraction.plot(ax=axes[1], kind='hist', x='cdf', normed=True, cumulative=True, range=(0,.3))
plt.show()

## percent-bachelors

lectures = pd.read_csv('data/percent-bachelors-degrees-women-usa.csv',index_col=0)
print(lectures.head())

# Print the minimum value of the Engineering column
print(lectures[["Engineering"]].min())

# Print the maximum value of the Engineering column
print(lectures[["Engineering"]].max())

# Construct the mean percentage per year: mean
mean = lectures.mean(axis="columns")

# Plot the average percentage per year
mean.plot()

# Display the plot
plt.show()

## box plots

# Print summary statistics of the fare column with .describe()
print(titanic.fare.describe())

# Generate a box plot of the fare column
titanic.fare.plot(kind='box')

# Show the plot
plt.show()

## box plots by category

life_ = pd.read_csv('data/life_expectancy_at_birth.csv',index_col=0)

# Print the number of countries reported in 2015
print(life_.loc[:,'2015'].count())

# Print the 5th and 95th percentiles
print(life_.quantile([0.05,0.95]))

# Generate a box plot
years = ['1800','1850','1900','1950','2000']
life_[years].plot(kind='box')
plt.show()

