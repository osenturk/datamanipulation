# Define echo_word as a lambda function: echo_word
echo_word = (lambda word1,echo: word1 * echo)

# Call echo_word: result
result = echo_word('hey',5)

# Print result
print(result)


## Lambda, map
spells = ["protego", "accio", "expecto patronum", "legilimens"]

print("map")
# Use map() to apply a lambda function over spells: shout_spells
shout_spells = map(lambda spell: spell+'!!!', spells)

print("after map-lambda")
print(shout_spells)
# Convert shout_spells to a list: shout_spells_list
shout_spells_list = list(shout_spells)

print("after map-lambda list conversion:")
# Convert shout_spells into a list and print it
print(shout_spells_list)

## Lambda, filter
fellowship = ['frodo', 'samwise', 'merry', 'pippin', 'aragorn', 'boromir', 'legolas', 'gimli', 'gandalf']


# Use filter() to apply a lambda function over fellowship: result
result = filter(lambda member: len(member)>6, fellowship)

print("after filter lambda:")
# Convert result to a list: result_list
result_list = list(result)

# Convert result into a list and print it
print(result_list)

## Lambda, reduce

from functools import reduce

# Create a list of strings: stark
stark = ['robb', 'sansa', 'arya', 'brandon', 'rickon']

# Use reduce() to apply a lambda function over stark: result
result = reduce(lambda s,y:s+y, stark)

print("after reduce lambda:")
# Print the result
print(result)

## try catch

# Define shout_echo
def shout_echo(word1, echo=1):
    """Concatenate echo copies of word1 and three
    exclamation marks at the end of the string."""

    # Initialize empty strings: echo_word, shout_words

    echo_word = ""
    shout_words = ""
    # Add exception handling with try-except
    try:
        # Concatenate echo copies of word1 using *: echo_word
        echo_word = word1 * echo

        # Concatenate '!!!' to echo_word: shout_words
        shout_words = echo_word + "!!!"
    except:
        # Print error message
        print("word1 must be a string and echo must be an integer.")

    # Return shout_words
    return shout_words


# Call shout_echo
shout_echo("particle", echo="accelerator")

## try catch

# Define shout_echo
def shout_echo(word1, echo=1):
    """Concatenate echo copies of word1 and three
    exclamation marks at the end of the string."""

    # Raise an error with raise
    if echo<0:
        raise ValueError('echo must be greater than 0')

    # Concatenate echo copies of word1 using *: echo_word
    echo_word = word1 * echo

    # Concatenate '!!!' to echo_word: shout_word
    shout_word = echo_word + '!!!'

    # Return shout_word
    return shout_word

# Call shout_echo
shout_echo("particle", echo=5)

## Lambda: Filter: Select retweets from the Twitter DataFrame: result
import pandas as pd
tweets_df = pd.read_csv('data/tweets.csv')

result = filter(lambda x: x[0:2]=='RT', tweets_df['text'])

# Create list from filter object result: res_list
res_list = list(result)

# Print all retweets in res_list
for tweet in res_list:
    print(tweet)

